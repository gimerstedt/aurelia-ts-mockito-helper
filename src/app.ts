import { Service } from "./service";

export class App {
  static inject = [Service];

  message = "Hello World!";

  constructor(private service: Service) {}

  attached() {
    this.message = this.service.doit() === "yup" ? this.message : "nope";
  }
}
