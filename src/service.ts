import { Api } from "./api";

export class Service {
  // could use static inject, @inject or @autoinject, doesn't matter
  static inject = [Api];

  constructor(private api?: Api) {}

  doit(): string {
    return this.api.stuff();
  }
}
