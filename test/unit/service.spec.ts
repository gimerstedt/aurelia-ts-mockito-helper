import { Service } from "../../src/service";
import { Api } from "../../src/api";
import { anything, instance, mock, when } from "ts-mockito";

const mockClass = clazz => {
  const ret: any = {
    instances: []
  };

  for (const c of clazz.inject) {
    const m = mock(c);
    const i = instance(m);

    ret[c.name] = m;
    ret.instances.push(i);
  }

  // type: object with keys corresponding to the arguments
  // of clazz constructor or typeof clazz.inject (spread)
  // with instances being typeof clazz.inject
  //
  // or a completely different solution
  return ret;
};

describe("service", () => {
  let sut: Service;
  let mocks: any;

  beforeEach(() => {
    mocks = mockClass(Service);
    sut = new Service(...mocks.instances);
  });

  it("does things", () => {
    when(mocks.Api.stuff()).thenReturn("meep");
    expect(sut.doit()).toEqual("meep");
  });
});
