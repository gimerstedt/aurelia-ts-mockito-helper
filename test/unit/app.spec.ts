import { App } from "../../src/app";
import { Service } from "../../src/service";
import { Api } from "../../src/api";

describe("the app", () => {
  it("says hello", () => {
    const api = new Api();
    const service = new Service(api);
    expect(new App(service).message).toBe("Hello World!");
  });
});
